package arena.game;

public class MinorityGame extends AbstractSingleValueGame {
	@Override
	public int generateOutcome() {
		int[] counts = new int[2];
		for (int move : moves.values()) {
			counts[move]++;
		}
		
		return counts[0] > counts[1] ? 1:0;
	}
}
