package arena.game;

import java.util.Set;

public interface Game {
	public void registerPlayer(String agentName);
	public Set<String> getPlayers();
	public int numPlayers();

	public int numMoves();
	public void clearMoves();
	public void makeMove(String name, int move);
	public int generateOutcome();
	public RoundState getState();
}
