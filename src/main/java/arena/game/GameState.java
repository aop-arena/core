package arena.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GameState {
	public Set<String> players;
	public List<RoundState> rounds = new ArrayList<RoundState>();
}
