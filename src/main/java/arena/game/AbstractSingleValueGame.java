package arena.game;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class AbstractSingleValueGame implements Game {
	
	Map<String, Integer> moves = new TreeMap<String, Integer>();
	Set<String> players = new TreeSet<String>();
	int outcome;
	
	@Override
	public void makeMove(String name, int move) {
		if (!players.contains(name)) {
			throw new RuntimeException("Player is not registered with game");
		}
		moves.put(name, move);
	}
	
	public Set<String> getPlayers() {
		return players;
	}
	
	public RoundState getState() {
//		System.out.println("moves: " + moves);
		RoundState state = new RoundState();
		state.moves = new TreeMap<String, Integer>();
		
		int i=0;
		for (String player : players) {
			Integer move = moves.get(player);
			if (move == null) move = -1;
			state.moves.put(player, move);
		}
		state.outcome = new SingleValueOutcome(outcome);
		return state;
	}

	@Override
	public void registerPlayer(String name) {
		players.add(name);
	}

	@Override
	public int numPlayers() {
		return players.size();
	}

	@Override
	public int numMoves() {
		return moves.size();
	}

	@Override
	public void clearMoves() {
		moves.clear();
	}

	@Override
	public int generateOutcome() {
		return outcome = (int) Math.random()*2;
	}
}
