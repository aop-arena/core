package arena.game;

public class RandomGame extends AbstractSingleValueGame {
	@Override
	public int generateOutcome() {
		return outcome = (int) Math.random()*2;
	}
}
