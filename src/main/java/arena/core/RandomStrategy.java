package arena.core;

public class RandomStrategy extends AbstractStrategy {
	public RandomStrategy(int maxNumberOfMoves) {
		super(maxNumberOfMoves);
	}

	@Override
	public int generateMove() {
		return (int) (Math.random() * numberOfMoves);
	}
	
	@Override
	public void setOutcome(int outcome) {
	}
}
