package arena.core;

public interface Strategy {
	public void setNumberOfMoves(int numberOfMoves);
	
	public int generateMove();
	void setOutcome(int outcome);

}
