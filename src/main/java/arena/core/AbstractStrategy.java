package arena.core;

public abstract class AbstractStrategy implements Strategy {
	protected int maxNumberOfMoves;
	protected int numberOfMoves;
	
	public AbstractStrategy(int maxNumberOfMoves) {
		this.maxNumberOfMoves = maxNumberOfMoves;
		numberOfMoves = maxNumberOfMoves;
	}

	public void setNumberOfMoves(int numberOfMoves) { 
		if (numberOfMoves > maxNumberOfMoves)
			throw new RuntimeException("Number of Moves is too high");
		this.numberOfMoves = numberOfMoves;
	}
}
