package arena.core;

public class CopyCatStrategy extends AbstractStrategy {
	private int outcome = -1; // No outcome
	
	public CopyCatStrategy(int maxNumberOfMoves) {
		super(maxNumberOfMoves);
	}

	@Override
	public int generateMove() {
		return outcome == -1 ? (int) (Math.random() * numberOfMoves):outcome;
	}
	
	public void setOutcome(int outcome) {
		this.outcome = outcome;
	}
}
